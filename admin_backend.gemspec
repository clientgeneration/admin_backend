$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "admin_backend/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "admin_backend"
  s.version     = AdminBackend::VERSION
  s.authors     = ["Hong Lance Lui"]
  s.email       = ["honglui@Multiplier.io"]
  s.homepage    = "http://multiplier.io"
  s.summary     = "Summary of AdminBackend."
  s.description = "Description of AdminBackend."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]


  s.add_dependency "rails", ">= 4.0.0"

  s.add_development_dependency "sqlite3"
end
