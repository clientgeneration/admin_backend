var dataTableInit = function(){
	$('#dataTables-users').dataTable();
	$('#dataTables-accounts').dataTable();
};

$(document).on("ready", dataTableInit);
$(document).on("page:load", dataTableInit);