require_dependency "admin_backend/application_controller"

module AdminBackend
  class AccountsController < ApplicationController
  	before_filter :authenticate_user!
  	before_filter :authenticate_super_user?

    def index
    	@accounts = Account.all
    end

    def show
    	@account = Account.find(params[:id])
    	@users = @account.users
    end

    # creating a user that is an account owner
    def new
      @account = Account.new
    end

    # creating a user that is an account owner
    def create
      @account = Account.new(account_params)
      if @account.save
        redirect_to @account 
      else
        render action: "new"
      end
    end

    private
    def account_params
      params.require(:account).permit(:title, 
        :account_type, :description, :address_line1, :addressline2, :suburb, :post_code, :state, 
        :country, :phone, :email, :website )  
    end
  end
end


# get all records that belongs to account 
# ActiveRecord::Base.subclasses.each do |sc| 
#    if sc.column_names.include?('account_id') 
#           sc.where(account_id: @account.id).count
#     end 
# end 