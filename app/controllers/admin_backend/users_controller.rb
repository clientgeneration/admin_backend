require_dependency "admin_backend/application_controller"

module AdminBackend
  class UsersController < ApplicationController
  	before_filter :authenticate_user!
  	before_filter :authenticate_super_user?
  	def index
  		@users = User.all
  	end

  	def show
  		@user = User.find(params[:id])
  	end

    def new
      @account = Account.find(params[:account_id])
      @user = User.new
    end

    def create
      @account = Account.find(params[:account_id])
      @user = User.new(user_params)
      if @user.save
        redirect_to [@account, @user] 
      else  
        render action: "new"
      end
    end

    def activate
      @user = User.find(params[:id])
      @user.active = true
      @user.save

      redirect_to :back
    end

    def deactivate
      @user = User.find(params[:id])
      @user.active = false
      @user.save

      redirect_to :back
    end

    def set_account_owner
      @account = Account.find(params[:account_id])
      @user = User.find(params[:id])

      @user.account_owner = true
      @user.save

      @account.user_id = @user.id
      @account.save

      if @account.users.count > 1
        @account.users.where.not(id: @user.id).each do |user|
          if user.account_owner == true
            user.account_owner = false
            user.save
          end
        end
      end
      redirect_to :back
    end

    private
    def user_params
      if params[:action] == 'create'
        params[:user][:created_by]=current_user.id
        params[:user][:created_ip]=request.remote_ip
        params[:user][:account_id]=@account.id
      end
      params[:user][:updated_by]=current_user.id
      params[:user][:updated_ip]=request.remote_ip
      params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :account_id, :created_by, :created_ip, :updated_by, :updated_ip)  
    end
  end
end
