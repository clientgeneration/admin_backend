module AdminBackend
  class ApplicationController < ActionController::Base
	# Authenticate super user
	def authenticate_super_user?
		redirect_to main_app.root_path unless current_user.is_super_user?
	end  
	helper_method :authenticate_super_user?
  end
end
