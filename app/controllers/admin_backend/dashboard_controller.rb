require_dependency "admin_backend/application_controller"

module AdminBackend
  class DashboardController < ApplicationController
  	before_filter :authenticate_user!
  	before_filter :authenticate_super_user?
    def index
    	@accounts = Account.all
    	@users = User.all
    end
  end
end
