require_dependency "admin_backend/application_controller"

module AdminBackend
  class ReportsController < ApplicationController
  	before_filter :authenticate_user!
  	before_filter :authenticate_super_user?
    def index
    end
  end
end
