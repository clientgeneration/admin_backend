AdminBackend::Engine.routes.draw do
	root :to => 'dashboard#index'
	resources :accounts do
		resources :users do
			member do
				get 'activate'
				get 'deactivate'
				get 'set_account_owner'
			end
		end
	end
	get 'users', :to => 'users#index'
	get 'reports', :to => 'reports#index'
end
