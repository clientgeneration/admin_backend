module AdminBackend
  class Engine < ::Rails::Engine
    isolate_namespace AdminBackend
  end
end
